"use strict";

$(document).ready(function () {
	$(".b_rv_slider").slick({
		arrows: true,
		fade: true,
		nextArrow:
			'<button type="button" class="slick-next  rv-btn-next">></button>',
		prevArrow:
			'<button type="button" class="slick-prev  rv-btn-prev"><</button>',
		responsive: [
			{
				breakpoint: 992,
				settings: {
					nextArrow:
						'<button type="button" class="slick-next  item-btn-next">></button>',
					prevArrow:
						'<button type="button" class="slick-prev  item-btn-prev"><</button>',
				},
			},
		],
	});

	$(".b1_main-slider").slick({
		dots: true,
		arrows: true,
		fade: true,
		adaptiveHeight: true,
		nextArrow: '<button type="button" class="slick-next">></button>',
		prevArrow: '<button type="button" class="slick-prev"><</button>',
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					adaptiveHeight: true,
				},
			},
		],
	});

	$(".b1_mob-slider").slick({
		dots: true,
		arrows: false,
		// fade: true,
		// nextArrow: '<button type="button" class="slick-next">></button>',
		// prevArrow: '<button type="button" class="slick-prev"><</button>',
		// responsive: [
		// 	{
		// 		breakpoint: 1024,
		// 	},
		// ],
	});

	$(".b1_firms-slider").slick({
		dots: false,
		arrows: false,
		slidesToShow: 8,
		autoplay: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 7,
				},
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 5,
				},
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 4,
				},
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 2,
				},
			},
		],
	});

	$(".news_slider").slick({
		dots: false,
		arrows: false,
		nextArrow:
			'<button type="button" class="slick-next  item-btn-next">></button>',
		prevArrow:
			'<button type="button" class="slick-prev  item-btn-prev"><</button>',

		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: false,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					dots: true,
					arrows: true,
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 768,
				settings: {
					dots: true,
					arrows: true,
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 576,
				settings: {
					dots: true,
					arrows: true,
					slidesToShow: 1,
				},
			},
		],
	});

	$(".ts_slider").slick({
		dots: false,
		arrows: false,
		nextArrow:
			'<button type="button" class="slick-next  item-btn-next">></button>',
		prevArrow:
			'<button type="button" class="slick-prev  item-btn-prev"><</button>',

		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: false,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					dots: true,
					arrows: true,
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 768,
				settings: {
					dots: true,
					arrows: true,
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 576,
				settings: {
					dots: true,
					arrows: true,
					slidesToShow: 1,
				},
			},
		],
	});
});

// выпадание каталога

$(".hdr_main-nav_drop").on("mouseover", function () {
	$(".hdr_bdy_blackout").addClass("d-block");
});

$(".hdr_main-nav_drop").on("mouseout", function () {
	$(".hdr_bdy_blackout").removeClass("d-block");
});

//  коллапсы меню

$(".b1_mob-nav_arrow").on("click", function (e) {
	e.preventDefault();
	$(this).parent("div").parent("li").children("ul").slideToggle();
	$(this).toggleClass("b1_mob-nav_active-arrow");
});

// вызов мобильного меню

$(".hdr_mob-menu-btn").on("click", function () {
	$(".hdr_mob-nav-wrapper").addClass("hdr_active-mob-nav-wrapper");
	$(".mob_menu_back").addClass("d-block");
	$("body").addClass("breack_body");
});

$(".hdr_main-nav_close-btn").on("click", function () {
	$(".hdr_mob-nav-wrapper").removeClass("hdr_active-mob-nav-wrapper");
	$(".mob_menu_back").removeClass("d-block");
	$("body").removeClass("breack_body");
});

$(".mob_menu_back").on("click", function () {
	$(".hdr_mob-nav-wrapper").removeClass("hdr_active-mob-nav-wrapper");
	$(".mob_menu_back").removeClass("d-block");
	$("body").removeClass("breack_body");
});

// поле поиска

$(".hdr_serch-link").on("click", function (e) {
	e.preventDefault();
	$(".hdr_drop-search-form").slideToggle();
});

$(".hdr_close-serch").on("click", function () {
	$(".hdr_drop-search-form").slideToggle();
});

// faq коллапс

$(".faq_collapse-link").on("click", function (e) {
	e.preventDefault();
	$(this).parents(".faq_item").children(".faq_answer").slideToggle();
	$(this).parents(".faq_head").toggleClass("faq_head-opened");
});

// catalog коллапс

$(".filter_head a").on("click", function (e) {
	e.preventDefault();
	$(this).parents(".filter_item").children(".filter-body").slideToggle();
	$(this).parents(".filter_item").children(".fil_range-body").slideToggle();
	$(this).children(".filter-arrow").toggleClass("active-arrow");
});

//catalog modile filters

$(".filter-btn").on("click", function () {
	$(".filters_wrapper").fadeIn();
	$("body").addClass("breack_body");
});

$(".filter-ins-btn").on("click", function () {
	$(".filters_wrapper").fadeOut();
	$("body").removeClass("breack_body");
});
